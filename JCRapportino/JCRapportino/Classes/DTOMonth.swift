//
//  DTOMonth.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 11/11/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class DTOMonth: NSObject {
    var month:Int?
    var monthString:String?
    
    func toMonthString(date:Int) -> String {
        
        switch(date) {
        case 1:
            monthString = "Gennaio"
        case 2:
            monthString = "Febbraio"
        case 3:
            monthString = "Marzo"
        case 4:
            monthString = "Aprile"
        case 5:
            monthString = "Maggio"
        case 6:
            monthString = "Giugno"
        case 7:
            monthString = "Luglio"
        case 8:
            monthString = "Agosto"
        case 9:
            monthString = "Settembre"
        case 10:
            monthString = "Ottobre"
        case 11:
            monthString = "Novembre"
        case 12:
            monthString = "Dicembre"
        default:
            print("Il giorno non esiste!")
        }
        return monthString!
    }

}
