//
//  SignInViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 14/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldRepeatPassword: UITextField!
    
    var alert_emailSended = UIAlertView()
    let alert = UIAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Button
    @IBAction func buttonSignIn(sender: AnyObject) {
        
        alert.title = "ATTENZIONE"
        alert.delegate = self
        alert.addButtonWithTitle("OK")
        
        if textFieldEmail.text!.isEmpty {
            alert.message = "Inserisci l'email"
            alert.show()
            return
        }
        
        if textFieldPassword.text!.isEmpty {
            alert.message = "Inserisci la password"
            alert.show()
            return
        }
        
        if textFieldRepeatPassword.text!.isEmpty {
            alert.message = "Ripeti la password"
            alert.show()
            return
        }
        
        if !(textFieldPassword.text == textFieldRepeatPassword.text) {
            alert.message = "Le password non corrispondono"
            alert.show()
            return
        }
        
        super.startActivityIndicator()
        WSRequest.executeRegistration(email: self.textFieldEmail.text!, password: self.textFieldPassword.text!) { [weak self] (request, response, data, error) -> () in
            
            self?.stopActivityIndicator()
            
            if let error_: NSError = error {
                // ERROR
                print("ERROR - request: \(request)")
                print("ERROR - response: \(response)")
                print("ERROR - data: \(data)")
                print("ERROR - error: \(error_)")
                
            } else {
                self!.alert_emailSended.title = "AVVISO"
                self!.alert_emailSended.message = "Ti abbiamo inviato una mail di conferma, conferma attraverso il link riportato nella mail."
                self!.alert_emailSended.delegate = self
                self!.alert_emailSended.addButtonWithTitle("OK")
                self!.alert_emailSended.show()
            }
        }
    } 
    
    // Metodo che rileva la pressione del pulsante su AlertView
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView == alert_emailSended {
            if buttonIndex == 0 {
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
}