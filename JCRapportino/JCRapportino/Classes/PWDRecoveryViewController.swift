//
//  PWDRecoveryViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 06/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class PWDRecoveryViewController: BaseViewController {
    
    @IBOutlet weak var TextFieldEmail: UITextField!
    let alert_emptyMail = UIAlertView()
    let alert_emailSended = UIAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Password Recovery"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Actions
    @IBAction func buttonPWDRecovery(sender: AnyObject) {
        
        if TextFieldEmail.text!.isEmpty {
            alert_emptyMail.title = "ATTENZIONE"
            alert_emptyMail.message = "Inserisci le informazioni richieste"
            alert_emptyMail.delegate = self
            alert_emptyMail.addButtonWithTitle("OK")
            alert_emptyMail.show()
        } else {
            alert_emailSended.title = "AVVISO"
            alert_emailSended.message = "Ti abbiamo inviato una nuova password, controlla l'email"
            alert_emailSended.delegate = self
            alert_emailSended.addButtonWithTitle("OK")
            alert_emailSended.show()
        }
    }
    // Metodo che rileva la pressione del pulsante su AlertView
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView == alert_emailSended {
            if buttonIndex == 0 {
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
}
