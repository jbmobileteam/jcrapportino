//
//  BaseViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 07/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UIAlertViewDelegate {
    
    var authenticated = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        showActivityIndicator()
        activityIndicatorSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK: - Setups
    private func activityIndicatorSetup() {
        // creo un unico activityIndicator con tag 999 richiamabile da tutta l'app
        if AppDelegate.sharedInstance.window?.viewWithTag(999) == nil {
            //var activityIndicator: UIActivityIndicatorView?
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            
            let width = CGFloat(100.0)
            let height = CGFloat(100.0)
            let x = (UIScreen.mainScreen().bounds.size.width/2) - (width/2)
            let y = CGFloat(100.0)
            
            activityIndicator.frame = CGRectMake(x, y, width, height)
            activityIndicator.tag = 999
            AppDelegate.sharedInstance.window?.addSubview(activityIndicator)
        }
    }
    
    private func showActivityIndicator() {
        if let activityIndicator = AppDelegate.sharedInstance.window?.viewWithTag(999) as? UIActivityIndicatorView {
            AppDelegate.sharedInstance.window?.bringSubviewToFront(activityIndicator)
        }
    }
    
    func startActivityIndicator() {
        if let activityIndicator = AppDelegate.sharedInstance.window?.viewWithTag(999) as? UIActivityIndicatorView {
            AppDelegate.sharedInstance.window?.bringSubviewToFront(activityIndicator)
            activityIndicator.startAnimating()
        }
    }
    
    func stopActivityIndicator() {
        if let activityIndicator = AppDelegate.sharedInstance.window?.viewWithTag(999) as? UIActivityIndicatorView {
            activityIndicator.stopAnimating()
        }
    }
    
    
}
