//
//  SecondViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 02/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit
import Foundation

class NewsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var dataSource:[AnyObject]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        dataSource = ["Comunicazione 1", "Comunicazione 2", "Comunicazione 3", "Comunicazione 4"]
        dataSource = Array()
        checkForNews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource!.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        cell?.textLabel?.text = (dataSource![indexPath.row] as? DTONews)?.title
        cell?.detailTextLabel?.text = (dataSource![indexPath.row] as? DTONews)?.content
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let newsDetail = self.storyboard?.instantiateViewControllerWithIdentifier("NewsDetailViewController") as! NewsDetailViewController
        
        newsDetail.titleLabel = (dataSource![indexPath.row] as! DTONews).title
        newsDetail.content = (dataSource![indexPath.row] as! DTONews).content
        self.navigationController?.pushViewController(newsDetail, animated: true)
    }
    
    func checkForNews() {
        super.startActivityIndicator()
        WSRequest.executeCheckForNews { [weak self] (request, response, data, error) -> () in
            
            if let _self = self {
                _self.stopActivityIndicator()
            }
            
            if let error_: NSError = error {
                // ERROR
                print("ERROR - request: \(request)")
                print("ERROR - response: \(response)")
                print("ERROR - data: \(data)")
                print("ERROR - error: \(error_)")
            } else {
                if let _self = self {
                    if let data_ = data!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
//                        print("\(data_)")
                        
                        let json = JSON(data: data_)
                        
                        if let news_ = json["news"].array {
                            for targetDic in news_ {
                                if let dictionary = targetDic.dictionary {
                                    let dto:DTONews = DTONews()
                                    dto.title = dictionary["title"]!.string!
                                    dto.content = dictionary["content"]!.string!
                                    _self.dataSource?.append(dto)
                                }
                            }
                            _self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
}

