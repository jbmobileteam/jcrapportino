//
//  NewsDetailViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 14/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {

    @IBOutlet weak var labelTitolo: UILabel!
    @IBOutlet weak var textView: UITextView!
    var titleLabel:String?
    var content:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelTitolo.text = titleLabel
        textView.text = content
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
