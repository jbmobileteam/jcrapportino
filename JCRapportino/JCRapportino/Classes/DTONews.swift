//
//  DTONews.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 09/11/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class DTONews: NSObject {
    var idNews:Int?
    var title:String?
    var content:String?
}
