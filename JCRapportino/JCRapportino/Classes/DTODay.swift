//
//  DTODay.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 10/11/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class DTODay: NSObject {
    var day:Int?
    var weekDay:Int?
    var weekDayString:String?
    
    func toDayString(date:Int) {
        
        switch(date) {
        case 1:
            weekDayString = "Domenica"
        case 2:
            weekDayString = "Lunedì"
        case 3:
            weekDayString = "Martedì"
        case 4:
            weekDayString = "Mercoledì"
        case 5:
            weekDayString = "Giovedì"
        case 6:
            weekDayString = "Venerdì"
        case 7:
            weekDayString = "Sabato"
        default:
            print("Il giorno non esiste!")
        }
    }
}
