//
//  User+CoreDataProperties.swift
//  JCRapportino
//
//  Created by David Iarriccio on 02/10/15.
//  Copyright © 2015 JobConsultance. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var name: String?
    @NSManaged var surname: String?
    @NSManaged var id_: NSNumber?

}
