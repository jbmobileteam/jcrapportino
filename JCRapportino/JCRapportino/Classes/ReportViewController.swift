//
//  FirstViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 02/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit
import Foundation

class ReportViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    let dtoM = DTOMonth()
    var date = NSDate()
    let calendar = NSCalendar.currentCalendar()
    var components:NSDateComponents?
    var monthString:String?
    
    var dataSource:[AnyObject]?

    override func viewDidLoad() {
        super.viewDidLoad()
        let components = calendar.components([.Hour, .Minute, .Month, .Year, .Day, .Weekday], fromDate: date)
        
        self.title = "Rapportino di \(dtoM.toMonthString(components.month))"

        dataSource = Array()
        calculateMonthForDays()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: TableView Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        cell?.textLabel?.text = "\((dataSource![indexPath.row] as? DTODay)!.weekDayString!) \((dataSource![indexPath.row] as? DTODay)!.day!)"
        cell?.detailTextLabel?.text = monthString
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let dayViewController = self.storyboard?.instantiateViewControllerWithIdentifier("dayViewController") as! DayViewController
        
        if ((dataSource![indexPath.row] as? DTODay)!.weekDay! != 1 && (dataSource![indexPath.row] as? DTODay)!.weekDay! != 7) {
            print("\((dataSource![indexPath.row] as? DTODay)!.weekDay!)")
            dayViewController.stringGiorno = "\((dataSource![indexPath.row] as? DTODay)!.day!) \(monthString!)"
            self.navigationController?.pushViewController(dayViewController, animated: true)
        }
    }
    
    func calculateMonthForDays() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        components = calendar.components([.Hour, .Minute, .Month, .Year, .Day, .Weekday], fromDate: date)
        let days = calendar.rangeOfUnit(.Day, inUnit: .Month, forDate: date).length
        monthString = dtoM.toMonthString(components!.month)
        
        for var i = 0; i < days; i++ {
            let dto = DTODay()
            date = dateFormatter.dateFromString("\(i+1)/11/2015")!
            components = calendar.components([.Hour, .Minute, .Month, .Year, .Day, .Weekday], fromDate: date)
            dto.weekDay = components!.weekday
            dto.day = i+1
            dto.toDayString(components!.weekday)
            self.dataSource?.append(dto)
        }
    }
    
    
}

