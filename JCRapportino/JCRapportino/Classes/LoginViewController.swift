//
//  LoginViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 06/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var buttonProceed: UIButton!
    let alert = UIAlertView()
    
    @IBOutlet weak var switchAuthentication: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switchAuthentication.setOn(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Actions
    @IBAction func pushButtonProceed(sender: AnyObject) {
        
        alert.title = "ATTENZIONE"
        alert.delegate = self
        alert.addButtonWithTitle("OK")
        
        if ((textFieldEmail.text == nil) && (textFieldPassword.text == nil)) {
            buttonProceed.enabled = false
        }
        
        if textFieldEmail.text!.isEmpty {
            alert.message = "Inserisci l'email"
            alert.show()
            return
        }
        
        if textFieldPassword.text!.isEmpty {
            alert.message = "Inserisci la password"
            alert.show()
            return
            
        }
        
//        if Reachability.isConnectedToNetwork() == true {
            super.startActivityIndicator()
//            print("Connected!")
//        } else {
//            super.stopActivityIndicator()
//            print("Connection Failed!")
//            alert.title = "ATTENZIONE"
//            alert.message = "Connessione internet non disponibile!"
//            alert.show()
//        }
        
        WSRequest.executeLogin(username: self.textFieldEmail.text!, password: self.textFieldPassword.text!) { [weak self] (request, response, data, error) -> () in

            if let _self = self {
                _self.stopActivityIndicator()
            }
            
            if let error_: NSError = error {
                // ERROR
                print("ERROR - request: \(request)")
                print("ERROR - response: \(response)")
                print("ERROR - data: \(data)")
                print("ERROR - error: \(error_)")
            } else {
                if let JSON = data {
                    let success = JSON["success"]!?.boolValue
                    if let success_ = success {
                        if success_ {
                            let identityNumber_ = JSON["identity_number"]?!.intValue

                            AppDelegate.sharedInstance.identityNumber = Int(identityNumber_!)
                            print("\(AppDelegate.sharedInstance.identityNumber!)")
                            let uiViewController = self?.storyboard?.instantiateViewControllerWithIdentifier("showTabBar")
                            self?.presentViewController(uiViewController!, animated: true, completion: nil)
                        } else {
                            self!.alert.message = "nome utente o password non corretti"
                            self!.alert.show()
                        }
                    }
                    
                    print("\(JSON[success!])")
                }
            }
        }
    }
    
    @IBAction func switchRimaniConnesso(sender: AnyObject) {
        //TODO: persistere valore true/false
        let switchAut = sender as! UISwitch
        authenticated = switchAut.on
        print("\(authenticated)")
    }
    
    @IBAction func pushButtonSignIn(sender: AnyObject) {
        let uiViewController = self.storyboard?.instantiateViewControllerWithIdentifier("showSignInViewController")
        self.presentViewController(uiViewController!, animated: true, completion: nil)
    }
    @IBAction func pushButtonPWDRecovery(sender: AnyObject) {
        
        let uiViewController = self.storyboard?.instantiateViewControllerWithIdentifier("showPWDRecovery")
        self.presentViewController(uiViewController!, animated: true, completion: nil)
    }
}
