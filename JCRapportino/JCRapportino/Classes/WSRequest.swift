//
//  WSRequest.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 09/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

let kWebServerURL = "http://localhost:8080/jcrapportinoweb"
let KWSRapportino = "/m/rapportino"
let KWSLogin = "/m/login"
let KWSRegistration = "/m/registration"
let KWSNews = "/m/news"
let kWSTimeoutIntervalForRequest = 60.0

private enum WebServiceType {
    case Login
    case Report
    case Registration
    case News
}

class WSRequest: NSObject {
    
    static func executeLogin(username username: String, password: String, completion: (request: NSURLRequest, response: NSHTTPURLResponse?, data: AnyObject?, error: NSError?) -> () ) {
        
        self.executeRequest(webServiceType: WebServiceType.Login, parameters: ["username": username, "password": password], completion: completion)
    }
    
    static func executeRegistration(email email: String, password: String, completion: (request: NSURLRequest, response: NSHTTPURLResponse?, data: AnyObject?, error: NSError?) -> () ) {
        
        self.executeRequest(webServiceType: WebServiceType.Registration, parameters: ["email": email, "password": password], completion: completion)
    }
    
    static func executeCheckForNews(completion: (request: NSURLRequest, response: NSHTTPURLResponse?, data: AnyObject?, error: NSError?) -> () ) {
        
        self.executeRequest(webServiceType: WebServiceType.News, parameters: nil, completion: completion)
    }
    
    static func executeSendReportData(reportData reportData: String, completion: (request: NSURLRequest, response: NSHTTPURLResponse?, data: AnyObject?, error: NSError?) -> () ) {
        
        self.executeRequest(webServiceType: WebServiceType.Report, parameters: ["jsonObject": reportData], completion: completion)
        
        
    }
    
    private static func executeRequest(webServiceType webServiceType: WebServiceType, parameters: [String:AnyObject]?, completion: (request: NSURLRequest, response: NSHTTPURLResponse?, data: AnyObject?, error: NSError?) -> () ) {
        
        let configuration: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        
        
        configuration.timeoutIntervalForRequest = kWSTimeoutIntervalForRequest
        
        //let manager = Manager(configuration: configuration)
        
        //print(parameters!)
        switch webServiceType {
            
        case .Login:
            Alamofire.request(.POST, kWebServerURL+KWSLogin, parameters: parameters!)
                .responseJSON { response in
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    
                    completion(request: response.request!, response: response.response!, data: response.result.value!, error: response.result.error);
            }
            
        case .Registration:
            Alamofire.request(.POST, kWebServerURL+KWSRegistration, parameters: parameters!)
                .responseJSON { response in
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    
                    completion(request: response.request!, response: response.response!, data: response.result.value!, error: response.result.error);
            }
            
        case .Report:
            Alamofire.request(.POST, kWebServerURL+KWSRapportino, parameters: parameters!)
                .responseJSON { response in
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    
                    completion(request: response.request!, response: response.response!, data: response.result.value!, error: response.result.error)
            }
            
        case .News:
            Alamofire.request(.POST, kWebServerURL+KWSNews, encoding: .JSON).responseString
                { response in switch response.result {
                case .Success(let JSON):
                    
                    print("Success with JSON: \(JSON)")
                    completion(request: response.request!, response: response.response!, data: JSON, error: response.result.error)
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                }
            }
        }
    }
}
