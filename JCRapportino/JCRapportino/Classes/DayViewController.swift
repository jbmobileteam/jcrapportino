//
//  DayViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 05/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class DayViewController: BaseViewController, UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    // MARK: IBOutlel
    @IBOutlet weak var textFieldCommessa: UITextField!
    @IBOutlet weak var textFieldOreOrdinarie: UITextField!
    @IBOutlet weak var textFieldOreStraordinarie1: UITextField!
    @IBOutlet weak var textFieldOreStraordinarie2: UITextField!
    @IBOutlet weak var textFieldFerie: UITextField!
    @IBOutlet weak var textFieldPermessi: UITextField!
    @IBOutlet weak var textFieldMalattia: UITextField!
    
    @IBOutlet weak var labelConteggioOre: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: Variable
    var stringGiorno:String?
    var stringOre:String = "0"
    var stringMinuti:String = "00"
    var stringCommessa:String = "Commessa 1"
    var isDataPickerOreMinuti = false
    let millisecondsFrom1970 = NSDate().timeIntervalSince1970
    
    var buttonSave = UIButton()
    var toolBar:UIToolbar?
    var pickerViewJob:UIPickerView?
    var pickerViewOreLavorate:UIPickerView?
    
    let pickerDataReport:(commessa:[String], ore:[String], minuti:[String]) = (
        ["Commessa 1", "Commessa 2","Commessa 3"],     //COMMESSA
        ["0", "1", "2", "3", "4", "5", "6", "7", "8"], //ORE
        ["00", "15", "30", "45"])                      //MINUTI
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "\(stringGiorno!)"
        
        //Settaggio BarButton
        buttonSave = UIButton(type: UIButtonType.System)
        buttonSave.setTitle("Save", forState: UIControlState.Normal)
        buttonSave.frame = CGRectMake(0, 0, 50, 50)
        buttonSave.addTarget(self, action: "saveReportButton", forControlEvents: UIControlEvents.TouchUpInside)
        let rightBarButtonItem = UIBarButtonItem(customView: buttonSave)
        self.navigationItem.setRightBarButtonItem(rightBarButtonItem, animated: true)
        
        
        
        pickerViewJob = UIPickerView()
        pickerViewOreLavorate = UIPickerView()
        
        // creazione toolbar per inserimento bottone per il dismiss della pickerView
        toolBar = UIToolbar()
        toolBar?.barStyle = UIBarStyle.Default
        toolBar?.translucent = true
        toolBar?.tintColor = UIColor(red: 76/255, green: 125/255, blue: 255/255, alpha: 1)
        toolBar?.sizeToFit()
        
        //creazione spazio per allineamento a destra del bottone
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: "doneButton")
        
        toolBar?.setItems([spaceButton, doneButton], animated: false)
        toolBar?.userInteractionEnabled = true
        
        inputDelegate()
        pickerViewJob?.dataSource = self
        pickerViewJob?.showsSelectionIndicator = true
        pickerViewOreLavorate?.dataSource = self
        pickerViewOreLavorate?.showsSelectionIndicator = true
        setTextFieldInput()
        
        let bottom = containerView.frame.height - scrollView.frame.height
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, bottom, 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("textFieldDidBeginEditing:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("textFieldDidEndEditing:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerViewJob {
            return pickerDataReport.commessa[row]
        } else {
            if component == 0 {
                return pickerDataReport.ore[row]
            } else {
                return pickerDataReport.minuti[row]
            }
        }
    }
    
    // MARK: UIPickerView Delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        if pickerView == pickerViewJob {
            return 1
        } else {
            return 2
        }
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerViewJob {
            return pickerDataReport.commessa.count
        } else {
            if component == 0 {
                return pickerDataReport.ore.count
            } else {
                return pickerDataReport.minuti.count
            }
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerViewJob {
            stringCommessa = pickerDataReport.commessa[row]
        } else {
            
            isDataPickerOreMinuti = true
            if component == 0 {
                stringOre = pickerDataReport.ore[row]
            } else {
                stringMinuti = pickerDataReport.minuti[row]
            }
        }
    }
    
    // MARK: Button Action
    func doneButton() {
        
        textFieldCommessa.text = stringCommessa
        print("\(stringOre):\(stringMinuti)")
        
        if textFieldOreOrdinarie.isFirstResponder() {
            textFieldOreOrdinarie.text = "\(stringOre):\(stringMinuti)"
        }
        if textFieldOreStraordinarie1.isFirstResponder() {
            textFieldOreStraordinarie1.text = "\(stringOre):\(stringMinuti)"
        }
        if textFieldOreStraordinarie2.isFirstResponder() {
            textFieldOreStraordinarie2.text = "\(stringOre):\(stringMinuti)"
        }
        if textFieldFerie.isFirstResponder() {
            textFieldFerie.text = "\(stringOre):\(stringMinuti)"
        }
        if textFieldPermessi.isFirstResponder() {
            textFieldPermessi.text = "\(stringOre):\(stringMinuti)"
        }
        if textFieldMalattia.isFirstResponder() {
            textFieldMalattia.text = "\(stringOre):\(stringMinuti)"
        }
        
        textFieldCommessa.resignFirstResponder()
        textFieldOreOrdinarie.resignFirstResponder()
        textFieldOreStraordinarie1.resignFirstResponder()
        textFieldOreStraordinarie2.resignFirstResponder()
        textFieldFerie.resignFirstResponder()
        textFieldPermessi.resignFirstResponder()
        textFieldMalattia.resignFirstResponder()
        
        managePickerSelection()
    }
    
    func saveReportButton() {
        self.startActivityIndicator()
        let identityNumber = AppDelegate.sharedInstance.identityNumber
        
        var reportData:[String:AnyObject] = [:]
        reportData["identityNumber"] = identityNumber
        reportData["insertionDate"] = millisecondsFrom1970
        reportData["job"] = textFieldCommessa.text
        reportData["ordinaryWork"] = textFieldOreOrdinarie.text
        reportData["overtimeEvening"] = textFieldOreStraordinarie1.text
        reportData["overtimeNight"] = textFieldOreStraordinarie2.text
        reportData["holidays"] = textFieldFerie.text
        reportData["permits"] = textFieldPermessi.text
        reportData["disease"] = textFieldMalattia.text
        
        let jsonObject = JSON(reportData)
        print(jsonObject.string)
        print(jsonObject.rawString()!)
        
        WSRequest.executeSendReportData(reportData: jsonObject.rawString()!) { [weak self] (request, response, data, error) -> () in
            self?.stopActivityIndicator()
            
            if let error_: NSError = error {
                // ERROR
                print("ERROR - request: \(request)")
                print("ERROR - response: \(response)")
                print("ERROR - data: \(data)")
                print("ERROR - error: \(error_)")
                
            } else {
                if let JSON = data {
                    let success = JSON["success"]!?.boolValue
                    if let success_ = success {
                        if success_ {
                            
                            let uiView = self?.storyboard?.instantiateViewControllerWithIdentifier("reportViewController") as! ReportViewController
                            self?.navigationController?.popToViewController(uiView, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: Container View Management
    func animateViewMoving(up:Bool, moveValue:CGFloat) {
        let movementDuration:NSTimeInterval = 0.2
        let movement:CGFloat = (up ? -moveValue : moveValue)
        UIView.beginAnimations("AnimateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = CGRectOffset(self.view.frame, 0, movement)
        UIView.commitAnimations()
    }
    
    // MARK: TextField Management
    func setTextFieldInput() {
        // settaggio input alla selezione della textfield
        textFieldCommessa.inputView = pickerViewJob
        textFieldCommessa.inputAccessoryView = toolBar
        
        // settaggio input nuova pickerView selezione ore alle altre textfield
        textFieldOreOrdinarie.inputView = pickerViewOreLavorate
        textFieldOreOrdinarie.inputAccessoryView = toolBar
        textFieldOreStraordinarie1.inputView = pickerViewOreLavorate
        textFieldOreStraordinarie1.inputAccessoryView = toolBar
        textFieldOreStraordinarie2.inputView = pickerViewOreLavorate
        textFieldOreStraordinarie2.inputAccessoryView = toolBar
        textFieldFerie.inputView = pickerViewOreLavorate
        textFieldFerie.inputAccessoryView = toolBar
        textFieldPermessi.inputView = pickerViewOreLavorate
        textFieldPermessi.inputAccessoryView = toolBar
        textFieldMalattia.inputView = pickerViewOreLavorate
        textFieldMalattia.inputAccessoryView = toolBar
    }
    
    func inputDelegate() {
        pickerViewJob?.delegate = self
        pickerViewOreLavorate?.delegate = self
        textFieldCommessa.delegate = self
        textFieldOreOrdinarie.delegate = self
        textFieldOreStraordinarie1.delegate = self
        textFieldOreStraordinarie2.delegate = self
        textFieldFerie.delegate = self
        textFieldPermessi.delegate = self
        textFieldMalattia.delegate = self
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        var increment = CGFloat(0)
        
        if UIScreen.mainScreen().bounds.size.height == 480 {
            increment = CGFloat(35)
        } else if UIScreen.mainScreen().bounds.size.height == 568 {
            increment = CGFloat(15)
        }
        
        switch (textField) {
        case textFieldCommessa:
            animateViewMoving(true, moveValue: 0)
        case textFieldOreOrdinarie:
            animateViewMoving(true, moveValue: 20 + increment)
        case textFieldOreStraordinarie1:
            animateViewMoving(true, moveValue: 40 + increment)
        case textFieldOreStraordinarie2:
            animateViewMoving(true, moveValue: 60 + increment)
        case textFieldFerie:
            animateViewMoving(true, moveValue: 100 + increment)
        case textFieldPermessi:
            animateViewMoving(true, moveValue: 190 + increment)
        case textFieldMalattia:
            animateViewMoving(true, moveValue: 230 + increment)
        default:
            animateViewMoving(true, moveValue: 0)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        var increment = CGFloat(0)
        
        if UIScreen.mainScreen().bounds.size.height == 480 {
            increment = CGFloat(20)
        }
        
        switch (textField) {
        case textFieldCommessa:
            animateViewMoving(false, moveValue: 0)
        case textFieldOreOrdinarie:
            animateViewMoving(false, moveValue: 20 + increment)
        case textFieldOreStraordinarie1:
            animateViewMoving(false, moveValue: 40 + increment)
        case textFieldOreStraordinarie2:
            animateViewMoving(false, moveValue: 60 + increment)
        case textFieldFerie:
            animateViewMoving(false, moveValue: 100 + increment)
        case textFieldPermessi:
            animateViewMoving(false, moveValue: 190 + increment)
        case textFieldMalattia:
            animateViewMoving(false, moveValue: 230 + increment)
        default:
            animateViewMoving(false, moveValue: 0)
        }
    }
    
    // MARK: Management Methods
    func managePickerSelection() {
        //Metodo che gestisce le selezioni errate --> | 0:00 | 8:15 | 8:30 | 8:45 |
        if pickerDataReport.ore == ["8"] {
            if pickerDataReport.minuti != ["00"] {
                pickerViewOreLavorate?.selectRow(1, inComponent: 1, animated: true)
            }
        }
        
        if pickerDataReport.ore == ["0"] {
            if pickerDataReport.minuti == ["00"] {
                pickerViewOreLavorate?.selectRow(2, inComponent: 1, animated: true)
            }
        }
        
        for value in pickerDataReport.ore {
            if value == "" {
                
            }
        }
        //
        //        pickerViewJob?.selectRow(1, inComponent: 0, animated: true)
        //        pickerViewOreLavorate?.selectRow(1, inComponent: 0, animated: true)
        //        pickerViewOreLavorate?.selectRow(1, inComponent: 1, animated: true)
    }
    
    func convertDictionaryToString(dataReport:Dictionary<String, AnyObject>) -> String {
        do {
            let json = try NSJSONSerialization.dataWithJSONObject(dataReport, options: NSJSONWritingOptions())
            return ("\(json)")
        } catch {
            print("Qualcosa è andato storto!")
        }
        return ""
    }
}
