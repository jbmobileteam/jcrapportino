//
//  AccessViewController.swift
//  JCRapportino
//
//  Created by JOBConsultance Mobile Team on 06/10/15.
//  Copyright © 2015 JOBConsultance. All rights reserved.
//

import UIKit

class AccessViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // simula autenticazione
        
        var uiViewController: UIViewController?
        
        if (authenticated) {
            uiViewController = self.storyboard?.instantiateViewControllerWithIdentifier("showTabBar")
        } else {
            uiViewController = self.storyboard?.instantiateViewControllerWithIdentifier("showLoginViewController")
        }
        self.presentViewController(uiViewController!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
