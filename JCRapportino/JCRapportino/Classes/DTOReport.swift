//
//  DTOReport.swift
//  JCRapportino
//
//  Created by Giordano Duse on 19/11/15.
//  Copyright © 2015 JobConsultance. All rights reserved.
//

import UIKit

class DTOReport: NSObject {
    var identityNumber:Int?
    var insertionDate:Int?
    var job:String?
    var ordinaryWork:String?
    var overtimeEvening:String?
    var overtimeNight:String?
    var holidays:String?
    var permits:String?
    var disease:String?
}
